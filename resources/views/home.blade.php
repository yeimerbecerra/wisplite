@vite('resources/sass/app.sass')

<h1 class="text-green-500 text-center text-5xl my-class-test">
    Hello Wisplite!
</h1>

<div x-data="{ open: false }">
    <button @click="open = !open">Expand</button>
    <div x-show="open">
        Content...
    </div>
</div>

@vite('resources/js/app.js')