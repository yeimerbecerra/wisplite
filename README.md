# Instrucciones para configurar entorno:


## Dependencias:

Se recomienda utilizar las versiones especificadas para no tener problemas a la hora de crear el entorno.

- Docker version 23.0.1, build a5ee5b1

- Composer version 2.4.3

- Npm version 8.15.0

Recuerde tener instalado en su sistema:

- git version 2.25.1

## Pasos para levantar entorno:


Una ves clonado el repositorio de **wisplite** ingresar en la carpeta y ejecutar el siguiente comando:

`sail up`

Comenzaría el proceso de construcción de los contenedores y imágenes de la app.

Al culminar, este proceso suele tardar varios minutos dependiendo de la velocidad de internet, se debe ejecutar los siguientes comandos en el mismo orden:

`composer install`


`npm install`


Donde aparecer la ruta, del servidor local. http://localhost/






